import React from 'react';
import { EmailIcon, PhoneIcon, GithubIcon, NpmIcon, JuejinIcon } from './components/icons';
import './App.css';

function Resume() {
  return (
    <div className="resume">
      <div className="side">
        <div className="flex flex-c items-center">
          <img
            className="img"
            src="https://avatars.githubusercontent.com/u/34960995?v=4"
            alt="avatars"
            width={100}
            height={100}
          />
          <h2 className="name">万川琳</h2>
          <h2 className="job">求职岗位： 前端研发</h2>
        </div>
        <div className="side-split" />
        <div className="icon-list">
          <a className="email icon-item" href="mailto:w2239559319@outlook.com">
            <EmailIcon />
            <div>
              <p className="icon-label">邮箱</p>
              <p className="icon-value">w2239559319@outlook.com</p>
            </div>
          </a>
          <a className="phone icon-item" href="tel:13072860887">
            <PhoneIcon />
            <div>
              <p className="icon-label">电话</p>
              <p className="icon-value">13072860887</p>
            </div>
          </a>
        </div>
        <div className="side-split" />
        <div className="socials">
          <a className="social-item npm" href="https://www.npmjs.com/~w2239559319">
            <NpmIcon />
            <div>
              <p className="social-label">npm</p>
              <p className="social-value">@xiaochuan</p>
            </div>
          </a>
          <a className="social-item github" href="https://github.com/2239559319">
            <GithubIcon />
            <div>
              <p className="social-label">github</p>
              <p className="social-value">@xiaochuan</p>
            </div>
          </a>
          <a className="social-item juejin" href="https://juejin.cn/user/184373686065111">
            <JuejinIcon />
            <div>
              <p className="social-label">稀土掘金</p>
              <p className="social-value">@一只小川</p>
            </div>
          </a>
        </div>
      </div>
      <div className="main">
        <div className="section">
          <div className="section-title">教育信息</div>
          <div className="section-main flex flex-w content-b">
            <div>
              2017.9-2021.6
            </div>
            <div>四川大学</div>
            <div>计算机科学与技术</div>
          </div>
        </div>
        <div className="section">
          <div className="section-title">工作经历</div>
          <div className="exp">
            <div className="exp-title">1. 字节跳动    北京   实习     前端开发    2020.9-2021.2</div>
            <div>于头条搜索前端团队实习，主要负责头条搜索页面部分的开发</div>
          </div>
          <div className="exp">
            <div className="exp-title">2. 字节跳动      上海     前端开发        2021.6-2023-7</div>
            <div>
              <div>负责头条搜素页面的部分研发。头条搜索是面向c端用户的一款产品，包括移动端，app端内以及pc端的应用。使用react以及nodejs作为技术栈，包括头条搜索首页以及搜索结果页和一些搜索结果的链接页面。搜索的结果页面包含不同种类的卡片信息，通过不同用户的推荐搜索算法来动态返回不同的内容</div>
              <div>
                <div>1. 通过分卡片的形式渲染加快首屏速度同时浏览器有空闲处理其他用户事件。负责游戏民生体育类卡片的业务开发</div>
                <div>2. 使用ssr的流式渲染加快首屏渲染的速度，使用a标签代替事件加快用户响应时间</div>
                <div>3. 使用pnpm提升依赖安装速度</div>
                <div>4. 通过定时发包的方法提供sdk给其他人使用</div>
                <div>5. 和客户端一起共建js bridge，输出sdk</div>
              </div>
            </div>
          </div>
          <div className="exp">
            <div className="exp-title">3. 字节跳动      成都     前端开发        2024.1-至今</div>
            <div>
              <div>负责内部saas平台的日常业务。飞书meego是一个对面企业的流程管理saas平台，包括流程的配置，人员的分配，流程节点的配置，流程相关表单项的配置以及流程实例的展示，流程实例包括流程图以及相关的信息</div>
              <div>
                <div>1. 开发浏览器mock插件，本地调试时代理到插件支持的mock数据，方便定位线上问题</div>
                <div>2. 负责表单控件的迁移，重构表单使用formik作为表单框架，方便管理状态定义validate以及元数据</div>
              </div>
            </div>
          </div>
        </div>
        <div className="section">
          <div className="section-title">技能</div>
          <div className="flex content-b">
            <div>h5/css/es6</div>
            <div>typescript</div>
            <div>nodejs</div>
            <div>react</div>
            <div>webpack</div>
            <div>npm/yarn/pnpm</div>
          </div>
        </div>
        <div className="section">
          <div className="section-title">项目</div>
          <div className="pro">
            <div className="pro-title">1. 头条搜素前端内部打包工具（内部项目）</div>
            <div>
              字节跳动头条搜素前端内部使用的组件库打包工具，包括打包，测试，部署等功能，实现多功能快速开发。打包模块用rollup进行打包，实现sourcemap快速上传至平台管理并使用，有利于快速定位。实现serverless函数收集平台报错并定时发布到群。
            </div>
          </div>
          <div className="pro">
            <div className="pro-title">2. koa-history（个人项目）</div>
            <div>koa的一个插件，支持history路由。支持router配置文件，实现动态路由等功能</div>
          </div>
          <div className="pro">
            <div className="pro-title">3. yarn-clean（个人项目）</div>
            <div>清除yarn长期未使用的缓存文件。通过查找yarn cache文件最后一次被访问的时间删除长期没有访问的包cache</div>
          </div>
          <div className="pro">
            <div className="pro-title">4. react官方文档</div>
            <div>补齐useId的使用场景，修复renderToString的类型</div>
          </div>
          <div className="pro">
            <div className="pro-title">5. umi（开源贡献）</div>
            <div>修复umi的target配置时机，umi第一次打包没有target所以会全部注入corejs，在dev时还会导致第一次编译的文件和第二次不一样重新触发mfsu。umi-father支持sourcemap。</div>
          </div>
          <div className="pro">
            <div className="pro-title">6. pnpm（开源贡献）</div>
            <div>修复workspace的依赖链过长导致lifecycle script的执行顺序出问题。修复node-linker=hoisted时autoInstallPeers不生效的问题</div>
          </div>
          <div className="pro">
            <div className="pro-title">7. acro-design（开源贡献）</div>
            <div>修复日历组件的UI问题。重构Slider的hooks修复hooks依赖问题。</div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Resume;
