export function GithubIcon() {
  return (
    <img src="https://github.githubassets.com/favicons/favicon.png" alt="github logo" />
  );
}
