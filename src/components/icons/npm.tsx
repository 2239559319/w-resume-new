export function NpmIcon() {
  return (
    <img src="https://static.npmjs.com/58a19602036db1daee0d7863c94673a4.png" alt="npm logo" />
  );
}
